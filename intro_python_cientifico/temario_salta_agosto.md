Charla Introducción a Python Científico: Jupyter Notebook y Pandas

Disertante: Horacio F. Mayo. Cátedra de Informatica Aplicada a la Biología, FCA-UNJu. Centro de Estudios en Bioestadística, Bioinformatica y Agromatica (CBBA), FCA-UNJu 

Duración: 2 horas

Lugar y Fecha: Aula Virtual, UNSa, Salta. Viernes 17 de agosto de 2018

Python Científico: Que es? Introducción. Librerías más usadas.  Por qué la programación puede ayudar a un científico? Papers y citas bibliográficas

Jupyter Notebook: Ipython. Instalación. Ventajas y Desventajas. Importancia en el ambiente científico. Manejo básico. Celdas. Kernel. Diapositivas. Ejemplos practicos.

Pandas: Introducción a Pandas. Usos más comunes. Series y Dataframes. Visualización de datos. Datos faltantes. Métodos de selección. Operaciones. Tabla dinamica. Graficos. Ejemplos practicos.