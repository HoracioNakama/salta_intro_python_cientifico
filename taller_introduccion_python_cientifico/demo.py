"Este es un modulo de prueba para el Talle de Introducción a Python Científico"

def print_b():
	"Prints b."
	print('b')

def print_a():
	"Prints a"
	print('a')

c = 2
d = 3
		
def promedio(*args, **kwargs):
    inicio = 0
    if len(args) == 0:
        print('Necesito dos números para poder operar')
    elif len(args) == 1:
        print('No puede hacerse un promedio con un sólo número')
    elif len(args)>1:
        for numero in args:
            try:
                inicio = inicio + float(numero)
            except ValueError:
                print("El elemento {} no es un número".format(numero))
        return inicio/len(args)

		
def promedio2(*args, **kwargs):
	inicio = 0
	if len(args)>1:
		for numero in args:
			inicio = inicio + numero
		return inicio/len(args)
	elif len(args) == 1:
		print('No puede hacerse un promedio con un solo número')
	elif len(args) == 0:
		print('Necesito dos números para poder operar')